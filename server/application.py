import tornado.web

from server.utils import MethodAndPathMatch
from server.handler import BodyHandler, StatisticHandler
from settings.general import db_client


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (MethodAndPathMatch('POST', r'/api/add'), BodyHandler),
            (MethodAndPathMatch('GET', r'/api/get'), BodyHandler),
            (MethodAndPathMatch('DELETE', r'/api/remove'), BodyHandler),
            (MethodAndPathMatch('PUT', r'/api/update'), BodyHandler),
            (MethodAndPathMatch('GET', r'/api/statistic'), StatisticHandler),
            # fixme: временное решение, эндпоинт не описанный в тз, удалить после решения проблемы с тестами
            (MethodAndPathMatch('DELETE', r'/api/statistic'), StatisticHandler),
        ]
        tornado.web.Application.__init__(self, handlers)
        self.db = db_client.get_default_database()
