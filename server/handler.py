from server.utils import generate_base64_key
from server.base import BaseHandler


class BodyHandler(BaseHandler):
    async def post(self):
        body = self.get_bytes_body_source()
        encoded_key = generate_base64_key(body)
        row = {'key': encoded_key, 'body': body}
        await self.db.queries.insert_one(row)
        self.write(encoded_key)

    async def get(self):
        db_key = self.get_arg('key')
        row = await self.db.queries.find_one({'key': db_key})
        duplicates = await self.db.queries.count_documents({'key': db_key})
        if not row:
            return self.send_error(400, reason="Body by this key doesn't exist")
        resp = {'body': row['body'], 'duplicates': duplicates}
        self.write(resp)

    async def delete(self):
        db_key = self.get_arg('key', 404)
        await self.db.queries.delete_many({'key': db_key})
        self.write({})

    async def put(self):
        body_request = self.get_bytes_body_source()
        if 'key' in body_request and 'body' in body_request:
            db_key, body = body_request['key'], body_request['body']
        else:
            return self.send_error(400, reason="Not found required arguments in requests: 'key', 'body'")

        new_encoded_key = generate_base64_key(body)
        new_row = {'$set': {'key': new_encoded_key, 'body': body}}
        await self.db.queries.update_one({'key': db_key}, new_row)
        self.write(new_encoded_key)


class StatisticHandler(BaseHandler):
    async def get(self):
        db_key = self.get_arg('key')
        key_duplicates = await self.db.queries.count_documents({'key': db_key})
        all_duplicates = await self.db.queries.count_documents({})
        self.write(str(round(key_duplicates / all_duplicates * 100, 2)))

    async def delete(self):
        await self.db.queries.delete_many({})

