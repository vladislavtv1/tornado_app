import base64

import tornado.routing


class MethodAndPathMatch(tornado.routing.PathMatches):
    def __init__(self, method, path_pattern):
        super().__init__(path_pattern)
        self.method = method

    def match(self, request):
        if request.method != self.method:
            return None

        return super().match(request)


def generate_base64_key(body: dict) -> str:
    """ Генерирует строку в base64 путем склеивания ключей и значений из словаря """
    key = ''.join([str(key) + str(value) for key, value in body.items()])
    return (base64.b64encode(key.encode('utf-8'))).decode('utf-8')
