import logging
import typing
import json

import tornado.web

logger = logging.getLogger(__name__)


class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        """ Инициализация базового обработчика запросов """
        ...

    def on_finish(self):
        """ Завершение обработки запроса """
        ...

    def write_error(self, status_code, **kwargs):
        """ Перехват ошибок возникающих через исключения """
        exc_info = kwargs['exc_info']
        exception = exc_info[1]
        result = {
            'errors': [str(exception)]
        }
        self.finish(result)

    def set_default_headers(self):
        """ Переопределенный метод установки ряда стандартных заголовков необходимых для CORS """

    def get_bytes_body_source(self) -> dict:
        """ Вернет словарь пришедших данных от клиента """
        try:
            body_arguments = tornado.escape.to_unicode(self.request.body)
            return tornado.escape.json_decode(body_arguments)
        except (TypeError, json.JSONDecodeError):
            return self.request.body.decode('utf-8')
        except UnicodeDecodeError:
            return {}

    def data_received(self, chunk):
        """ Переопределение для скрытия предупреждений """

    def get_arg(self, name: str, exc_code: int = 400) -> typing.Optional[str]:
        if name in self.args:
            return self.args[name][0].decode()
        else:
            logger.info(f'Bad request, argument {name} not found')
            self.send_error(exc_code, reason=f'Not found {name} argument in request')

    @property
    def db(self):
        return self.application.db

    @property
    def args(self):
        return self.request.arguments


