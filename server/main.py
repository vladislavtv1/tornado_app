import logging
import tornado.options
import tornado.httpserver
import tornado.ioloop

from server.application import Application


logger = logging.getLogger(__name__)


def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(tornado.options.options.port)
    tornado.ioloop.IOLoop.instance().start()
    logger.info('Application successfully started')


if __name__ == "__main__":
    main()
