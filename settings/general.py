import os

from tornado.options import define
from motor import motor_tornado


define('port', default=8000, help='run on the given port', type=int)

# MONGO_DOCKER_IP = '172.19.0.2'  # при отладке приходится использовать ip mongo контейнера
MONGO_DOCKER_IP = 'mongo'
MONGO_DEFAULT_URL = f'mongodb://root:root@{MONGO_DOCKER_IP}:27017/admin'
db_client = motor_tornado.MotorClient(os.environ.get('MONGODB_URL', MONGO_DEFAULT_URL))
