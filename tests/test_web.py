import json
import pytest
import tornado.httpclient
from tornado.httpclient import HTTPRequest

from server.utils import generate_base64_key
from server.application import Application


@pytest.fixture
def app():
    return Application


@pytest.fixture
def base_url() -> str:
    return 'http://0.0.0.0:8000/'


@pytest.fixture
def body() -> dict:
    return {"test_key": "test_value"}


@pytest.fixture
def urls(base_url) -> dict:
    return {
        'post': base_url + 'api/add',
        'get': base_url + 'api/get',
        'delete': base_url + 'api/remove',
        'put': base_url + 'api/update',
        'statistic': base_url + 'api/statistic',
    }


@pytest.mark.gen_test
def test_add_request(http_client, body, urls):
    # fixme: временный фикс по очистке бд, конечно перед каждым тестом бд должна быть пуста
    request = HTTPRequest(urls['statistic'], 'DELETE')
    response = yield http_client.fetch(request)
    assert response.code == 200

    expected_key = generate_base64_key(body)
    request = HTTPRequest(urls['post'], 'POST', body=json.dumps(body))
    response = yield http_client.fetch(request)
    assert response.code == 200
    assert response.body.decode("utf-8") == expected_key


@pytest.mark.gen_test
def test_get_request_not_existed_key(http_client, urls):
    request = HTTPRequest(urls['get'] + '?key=non-existed', 'GET')
    try:
        yield http_client.fetch(request)
        assert False
    except tornado.httpclient.HTTPClientError as exc:
        assert exc.code == 400


@pytest.mark.gen_test
def test_get_request_without_key(http_client, urls):
    request = HTTPRequest(urls['get'], 'GET')
    try:
        yield http_client.fetch(request)
        assert False
    except tornado.httpclient.HTTPClientError as exc:
        assert exc.code == 400


@pytest.mark.gen_test
def test_delete_request(http_client, urls):
    request = HTTPRequest(urls['delete'] + '?key=non-existed', 'DELETE')
    response = yield http_client.fetch(request)
    assert response.code == 200


@pytest.mark.gen_test
def test_crud_requests(http_client, body, urls):
    # очистка бд
    request = HTTPRequest(urls['statistic'], 'DELETE')
    response = yield http_client.fetch(request)
    assert response.code == 200

    # post -> one query in db
    request = HTTPRequest(urls['post'], 'POST', body=json.dumps(body))
    response = yield http_client.fetch(request)
    assert response.code == 200
    generated_key = response.body.decode("utf-8")

    # get by key -> success
    request = HTTPRequest(urls['get'] + f'?key={generated_key}', 'GET')
    response = yield http_client.fetch(request)
    assert response.code == 200
    expected_body = json.dumps({"body": body, "duplicates": 1})
    assert response.body.decode("utf-8") == expected_body

    # update -> updated query in db
    upd_body = {'key': generated_key, 'body': {'new_test_key': 'new_test_value'}}
    request = HTTPRequest(urls['put'], 'PUT', body=json.dumps(upd_body))
    response = yield http_client.fetch(request)
    assert response.code == 200
    new_generated_key = response.body.decode("utf-8")

    # get by old generated_key -> error
    request = HTTPRequest(urls['get'] + f'?key={generated_key}', 'GET')
    try:
        yield http_client.fetch(request)
        assert False
    except tornado.httpclient.HTTPClientError as exc:
        assert exc.code == 400

    # get by new generated_key -> success
    request = HTTPRequest(urls['get'] + f'?key={new_generated_key}', 'GET')
    response = yield http_client.fetch(request)
    assert response.code == 200

    # post -> two query in db
    request = HTTPRequest(urls['post'], 'POST', body=json.dumps(body))
    response = yield http_client.fetch(request)
    assert response.code == 200

    # statistic by old key -> 50%
    request = HTTPRequest(urls['statistic'] + f'?key={generated_key}', 'GET')
    response = yield http_client.fetch(request)
    assert response.code == 200
    assert float(response.body.decode("utf-8")) == 50.0

    # delete -> only updated query in db
    request = HTTPRequest(urls['delete'] + f'?key={generated_key}', 'DELETE')
    response = yield http_client.fetch(request)
    assert response.code == 200

    # statistic by old key -> 0%
    request = HTTPRequest(urls['statistic'] + f'?key={generated_key}', 'GET')
    response = yield http_client.fetch(request)
    assert response.code == 200
    assert float(response.body.decode("utf-8")) == 0.0
